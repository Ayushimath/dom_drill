// Select the section with an id of container without using querySelector
let containers = document.getElementById("container");
console.log(containers);

//- Select the section with an id of container using querySelector.
let first_container = document.querySelector("#container");
console.log(first_container);

//Select all of the list items with a class of "second"
let class_second = document.getElementsByClassName("second");
console.log(class_second);


//- Select a list item with a class of third, 
//but only the list item inside of the ol tag.
let third_ol = document.querySelector("ol");
console.log(third_ol.lastElementChild);


//- Give the section with an id of container the text "Hello!".
const new_section = document.getElementsByTagName("section");
new_section.id = "container"
new_section.text = "Hello!";

//Add the class main to the div with a class of footer.
const newmain = document.querySelector(".footer");
newmain.setAttribute("class", "main");
console.log(newmain);

//Remove the class main on the div with a class of footer
const newfooter = document.querySelector(".main");
newfooter.setAttribute("class", "footer");
console.log(newfooter);

//Create a new li element.
const newList = document.createElement("li");

//Give the li the text "four".
newList.innerText = "four";
console.log(newList);

//Append the li to the ul element.
const ul = document.querySelector("ul");
ul.append(newList);

//- Loop over all of the list inside the
// ol tag and give them a background color of "green"
let olList = document.querySelector("ol");
console.log(olList.children);

for (let i = 0; i < olList.children.length; i++) {
    console.log(olList.children[i]);
    (olList.children[i]).style.backgroundColor = "green";
}

//- Remove the div with a class of footer. 
const foot = document.querySelector(".footer");
console.log(foot);
foot.remove();


